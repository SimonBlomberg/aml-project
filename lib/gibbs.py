import scipy as sp
import numpy as np
import numpy.linalg as linalg
import matplotlib.pyplot as plt
from scipy.stats import truncnorm
from scipy.stats import multivariate_normal
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd


def create_gaussian(mean, variance):
    return {"mean" : mean, "variance" : variance}

def create_from_truncated_gaussian(a, b, original_gaussian):
    original_mean = original_gaussian['mean']
    original_variance = original_gaussian['variance']

    # Transform limits according to scipys implementation
    a = (a - original_mean) / np.sqrt(original_variance)
    b = (b - original_mean) / np.sqrt(original_variance)
    
    # Create a gaussian with the same mean and variance as the truncated gaussian
    mean = truncnorm.mean(a, b, scale=np.sqrt(original_variance), loc=original_mean)
    variance = truncnorm.var(a, b, scale=np.sqrt(original_variance), loc=original_mean)
    
    return create_gaussian(mean, variance)
            
def posterior_message_passing(y, s1, s2, diff_variance):
    # Starting message
    message_1 = s1 
    message_2 = s2 

    # Message going into W
    message_w = create_gaussian(message_1['mean'] - message_2['mean'],
            message_1['variance'] + message_2['variance'])

    # Message going into t
    message_t = create_gaussian(message_w['mean'], message_w['variance'] + diff_variance)

    # Truncate with result
    if y == 1:
        truncated_gaussian = create_from_truncated_gaussian(0, 10000, message_t)
    else:
        truncated_gaussian = create_from_truncated_gaussian(-10000, 0, message_t)

    def divide(g1, g2):
        mean = (g1['mean']*g2['variance'] - g2['mean']*g1['variance'])/(g1['variance'] - g2['variance'])
        variance =  (g1['variance']*g2['variance'])/(g2['variance'] - g1['variance'])
        return create_gaussian(mean, variance)


    tmp = divide(truncated_gaussian, message_t) 

    # Message into W
    message_w = create_gaussian(tmp['mean'], tmp['variance'] + diff_variance)
    
    message_s1 = create_gaussian(message_w['mean'] + message_2['mean'], 
            message_w['variance'] + message_2['variance'])

    message_s2 = create_gaussian(message_1['mean'] - message_w['mean'], 
            message_w['variance'] + message_1['variance'])

    def multiply(g1, g2):
        mean = (g1['mean']*g2['variance'] + g2['mean']*g1['variance'])/(g1['variance'] + g2['variance'])
        variance =  (g1['variance']*g2['variance'])/(g2['variance'] + g1['variance'])
        return create_gaussian(mean, variance)


    s1 = multiply(message_s1, message_1)
    s2 = multiply(message_s2, message_2)

    return (s1, s2)

def posterior_gibbs(y, variance, mean, diff_variance, samples=100, burn_in=100, t0 = 0, alpha = 0, beta = 0):
    """
        Uses gibbs sampling to calculate posterior distribution P(S|y).
    """
    M = np.array([[1, -1]])
    cov_s = np.array([[variance[0],           0],
                      [0,           variance[1]]])

    mu_s = np.array([[mean[0], mean[1]]]).T
    t = t0
    ss = np.zeros((samples, 2))
    ts = np.zeros((samples, 1))
    
    def update(t, alpha, beta):
        # Sample s
        cov = linalg.inv(linalg.inv(cov_s) + 1/(diff_variance**2) * M.T @ M)
        mu = cov @ (linalg.inv(cov_s) @ mu_s + M.T*t*(1/(diff_variance)))
        s = np.random.multivariate_normal(mean=mu[:,0], cov=cov) # draw from p(s|t=t, y=y0)

        # Sample t
        mean = (s[0] - s[1])
        truncated_normal = lambda a, b: truncnorm.rvs(a=(a-mean)/diff_variance, 
                                                            b=(b-mean)/diff_variance, 
                                                            loc=mean, 
                                                            scale=diff_variance)
        if y == 1:
            t = truncated_normal(alpha+beta, 50)
        elif y == 0 and alpha != 0:
            t = truncated_normal(-alpha+beta, alpha+beta)
        else:
            t = truncated_normal(-50, alpha+beta)
            
        return t, s

    for i in range(burn_in + samples):
        t, s = update(t, alpha, beta)
        if i >= burn_in:
            ss[i-burn_in,:] = s
            ts[i-burn_in,:] = t
    
    return ss, ts

def sample_y(mu1, mu2, sigma1, sigma2, sigmat, alpha, beta, draw_possible):
    s1 = np.random.normal(mu1, sigma1)
    s2 = np.random.normal(mu2, sigma2)
    
    t = np.random.normal((s1-s2), sigmat)
    if t < -alpha+beta:
        y = -1
    elif t > -alpha+beta and t < alpha+beta and draw_possible:
        y = 0
    else:
        y = 1
    return y

def predict_game(team1, team2, alpha, beta, draw_possible = False, sigmat = 1):
    samples = []
    for i in range(100):
        samples.append(sample_y(team1['mean'], team2['mean'], team1['variance'], team2['variance'], sigmat, alpha, beta, draw_possible))
    
    unique, counts = np.unique(samples, return_counts=True)
    return unique[np.argmax(counts)]

def approximate_gaussian(s):
    """
        Return approximated gaussians of the posteriors s
    """
    mean = np.mean(s, axis=0)
    variance = [np.var(s[:,0]), np.var(s[:,1])]

    normal1 = multivariate_normal(mean=mean[0], cov=variance[0])
    normal2 = multivariate_normal(mean=mean[1], cov=variance[1])
    return normal1, normal2

def plot_with_approx(ss):
    """
        Plot histograms of posteriors ss, with approximated gaussians overlayed. 
    """
    n1, n2 = approximate_gaussian(ss)
    x = np.linspace(10, 40, 100)
    s1 = n1.pdf(x)
    s2 = n2.pdf(x)

    plt.hist(ss[:,0], density=True, bins=25, alpha=0.5)
    plt.hist(ss[:,1], density=True, bins=25, alpha=0.5)

    plt.plot(x, s1)
    plt.plot(x, s2)

def plot_priors(sig_1, sig_2, mu_s1, mu_s2):
    """
        Plot the prior distributions of two team skills.
    """
    x = np.linspace(0, 100, 100)
    s1 = multivariate_normal.pdf(x, mean=mu_s1, cov=sig_1**2)
    s2 = multivariate_normal.pdf(x, mean=mu_s2, cov=sig_2**2)

    plt.plot(x, s1)
    plt.plot(x, s2)

def find_team_idx(team_name, teams):
    """
        Find index (in teams list) of team with name team_name.
    """
    for i, team in enumerate(teams):
        if team['name'] == team_name:
            return i
    return -1

def plot_team_hist(team, x0=20, x1=30, dashed=False):
    """
        Plot the team skill probility density function.
    """
    x = np.linspace(x0, x1, 1000)
    y = multivariate_normal.pdf(x, team['mean'], team['variance'])
    if dashed == True:
        plt.plot(x, y, '--')
    else:
        plt.plot(x, y)
    


def read_data(filename, remove_draw=True, sep=','):
    """
        Read teams and matches from csv file.
    """
    df = pd.read_csv(filename, sep=sep)
    team_names = df['team1'].unique()
    teams = []
    for t in team_names:
        teams.append({'name' : t, 'mean' : 25, 'variance' : 8.3})

    matches = []
    for idx,row in df.iterrows():
        if row['score1'] == row['score2'] and remove_draw:
            continue

        if row['score1'] > row['score2']:
            outcome = 1
        elif row['score1'] < row['score2']:
            outcome = -1
        else:
            outcome = 0
            
        matches.append({
                        'team1' : find_team_idx(row['team1'], teams), 
                        'team2' : find_team_idx(row['team2'], teams), 
                        'outcome' : outcome
                        })

    return teams, matches
# Note: Timings
# n_samples -> time [s]
# 1000 -> 0.784 s
# 10000 -> 2.761 s
# 50000 -> 11.689 s
# 100000 -> 23.117 s

# Note: Good burnin: 800-1000
# if __name__ == "__main__":
#     burn_in = 40
#     n_samples = 200
    
#     teams, matches = read_data("SerieA.csv")
#     for idx, m in enumerate(matches):
#         team1 = m['team1']
#         team2 = m['team2']
#         print(f"Match nr {idx}")
#         print(f"\tteam 1: {team1}\n\tteam 2: {team2}\n\toutcome: {m['outcome']}")
#         mu1 = teams[team1]['mean'] 
#         mu2 = teams[team2]['mean']
#         sig1 = teams[team1]['variance']
#         sig2 = teams[team2]['variance']
        
#         print(teams[team1]['variance'])
#         s, _ = posterior_gibbs(m['outcome'], variance=(sig1, sig2), mean=(mu1, mu2), diff_variance=1, samples=n_samples, burn_in=burn_in, t0=1)
#         teams[team1]['mean'] = np.mean(s[:,0])
#         teams[team1]['variance'] = np.var(s[:,0])
#         teams[team2]['mean'] = np.mean(s[:,1])
#         teams[team2]['variance'] = np.var(s[:,1])
#         print(teams[team1]['variance'])

#     for t in teams:
#         print(t)
#         plot_team_hist(t)

#     plt.show()
#     # ss, ts = posterior_gibbs(1, samples=n_samples, burn_in=burn_in, t0=0)
#     # plot_with_approx(ss)
#     # plot_priors(8.3, 8.3, 25, 25)
#     # plt.show()
